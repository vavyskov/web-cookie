/**
 * The file is for demo purposes,
 * don't use in your configuration!
 */

if(location.protocol.slice(0, 4) !== 'http'){
    var warning = document.createElement('p');
    //warning.innerHTML = '❗ If you see this message, it means that you are viewing this demo file directly! You need a webserver to test cookies! <br><br><em>Ensure that the URL begins with "<i>http</i>" rather than "<i>file</i>"!</em>';
    warning.innerHTML = '❗ Pokud vidíte tuto zprávu, znamená to, že si přímo prohlížíte tento ukázkový soubor! K testování cookies potřebujete webový server! <br><br><em>Ujistěte se, že adresa URL začíná "<i>http</i>" a ne "<i>soubor</i>"!</em>';
    warning.className = 'warning';
    document.body.appendChild(warning);
}

var cookieSettingsBtn = document.querySelector('[data-cc="c-settings"]');
var resetCookiesBtn = document.createElement('button');
resetCookiesBtn.type = 'button';
//resetCookiesBtn.innerText = 'Reset cookieconsent';
resetCookiesBtn.innerText = 'Resetetovat souhlas s cookies';
cookieSettingsBtn.parentNode.insertBefore(resetCookiesBtn, cookieSettingsBtn.nextSibling);

resetCookiesBtn.addEventListener('click', function(){
    cc.accept([]);
    cc.eraseCookies(['cc_cookie', 'cc_cookie_demo1', 'cc_cookie_demo2', 'cc_youtube']);
    window.location.reload();
});