# WEB Cookie

Rozšířená cookie lišta (od 1.1.2022)

Řešení:
- [Cookie Consent](https://www.cookieconsent.com/) 
  - neřeší &lt;iframe&gt; (mapa, video)
  - nelze napojit na “Google Tag Manager”, tak aby fungovaly kategorie souhlasů
  - je nutné vytvořit stránku s popisem všech cookies a do jaké kategorie patří
- [Orestbida - Cookie Consent](https://orestbida.com/demo-projects/cookieconsent/)
  - [Cookie lišta zdarma](https://www.incomaker.com/cs/blog/cookies-lista-zdarma)
- [Cookiebot](https://www.cookiebot.com/en/pricing/) - zdarma do 100 stránek
- [CookieHub](https://www.cookiehub.com/pricing) - zdarma do 25.000 jedinečných návštěvníků měsíčně
- [Cookie Script](https://cookie-script.com/pricing) - zdarma do 20.000 zobrazení stránek měsíčně
- [OneTrust](https://www.onetrust.com/pricing/) - komerční

Odkazy:
- [Cookie lišta, verze 2022](https://www.vzhurudolu.cz/prirucka/cookie-lista-2022)

Typy cookies:
- Technické (nezbytné)
- Analytické (cookies vs fingerprinting vs identifikace uživatele):
  - nástroje vyžadující cookie lištu
    - [Google Analytics](https://analytics.google.com/) - cookies ANO/NE, identifikace uživatele
    - [Matomo](https://matomo.org/) - fingerprinting
    - [Fanthom](https://usefathom.com/) - fingerprinting
  - nástroje nevyžadující cookie lištu
    - [Plausible](https://plausible.io/)
      - možnost použití i na vlastním serveru (Docker)
      - funguje i na AMP webech
    - [Simple Analytics](https://simpleanalytics.com/) - nesleduje uživatele napříč webem
    - [Fanthom](https://usefathom.com/)
    - [Microanalytics](https://microanalytics.io/)
    - [Umami](https://umami.is/)
    - [AnalytikaWebu.cz](https://analytikawebu.cz/)
    - [TOPlist](https://www.toplist.cz/)
- Marketingové (personalizační)

Komponenty třetích stran:
- Google fonts - externí stahování předává IP adresu - dá se vyřešit lokální kopií :)
- Twitter - ukládání personalizačních i reklamních cookies je možné vypnout :)
- Youtube - ukládá reklamní cookies (`youtube-nocookie.com` může být "fejk", cookies se ukládají po spuštění videa)
- Facebook embed (Facebook pixel) - ukládá cookies
- Disqus - ukládá cookies
- Mapy - https://github.com/01-Scripts/2Click-Iframe-Privacy (vynutí souhlas před zobrazením)
- &lt;iframe&gt; (např. mapy, videa) - obvykle ukládá cookies
  - [api.mapy.cz](https://api.mapy.cz/) - zdarma i pro komerční účely
  - [Iframe Manager](https://github.com/orestbida/iframemanager)
  - [2Click-Iframe-Privacy](https://github.com/01-Scripts/2Click-Iframe-Privacy)

Spuštění lokálního serveru:
- ```
  npx http-server
  ```
